<?php

namespace GsvPackages\Gsvadmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Exception;
use Log;
use DB;
use GsvPackages\Gsvadmin\Helpers\LAHelper;

class GsvAdminConfigs extends Model
{   
	protected $table = 'la_configs';
	
	protected $fillable = [
		"key", "value"
	];
	
	protected $hidden = [
		
	];

	// LAConfigs::getByKey('sitename');
	public static function getByKey($key) {
		$row = GsvAdminConfigs::where('key',$key)->first();
		if(isset($row->value)) {
			return $row->value;
		} else {
			return false;
		}
	}
	
	// LAConfigs::getAll();
	public static function getAll() {
		$configs = array();
		$configs_db = GsvAdminConfigs::all();
		foreach ($configs_db as $row) {
			$configs[$row->key] = $row->value;
		}
		return (object) $configs;
	}
}
