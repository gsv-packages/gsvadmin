<?php

/* ================== Homepage ================== */
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::auth();

/* ================== Access Uploaded Files ================== */
Route::get('files/{hash}/{name}', 'Gsvadmin\UploadsController@get_file');

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\GsvPackages\Gsvadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
	$as = config('gsvadmin.adminRoute').'.';
	
	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');
}

Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {
	
	/* ================== Dashboard ================== */
	
	Route::get(config('gsvadmin.adminRoute'), 'Gsvadmin\DashboardController@index');
	Route::get(config('gsvadmin.adminRoute'). '/dashboard', 'Gsvadmin\DashboardController@index');
	
	/* ================== Users ================== */
	Route::resource(config('gsvadmin.adminRoute') . '/users', 'Gsvadmin\UsersController');
	Route::get(config('gsvadmin.adminRoute') . '/user_dt_ajax', 'Gsvadmin\UsersController@dtajax');
	
	/* ================== Uploads ================== */
	Route::resource(config('gsvadmin.adminRoute') . '/uploads', 'Gsvadmin\UploadsController');
	Route::post(config('gsvadmin.adminRoute') . '/upload_files', 'Gsvadmin\UploadsController@upload_files');
	Route::get(config('gsvadmin.adminRoute') . '/uploaded_files', 'Gsvadmin\UploadsController@uploaded_files');
	Route::post(config('gsvadmin.adminRoute') . '/uploads_update_caption', 'Gsvadmin\UploadsController@update_caption');
	Route::post(config('gsvadmin.adminRoute') . '/uploads_update_filename', 'Gsvadmin\UploadsController@update_filename');
	Route::post(config('gsvadmin.adminRoute') . '/uploads_update_public', 'Gsvadmin\UploadsController@update_public');
	Route::post(config('gsvadmin.adminRoute') . '/uploads_delete_file', 'Gsvadmin\UploadsController@delete_file');
	
	/* ================== Roles ================== */
	Route::resource(config('gsvadmin.adminRoute') . '/roles', 'Gsvadmin\RolesController');
	Route::get(config('gsvadmin.adminRoute') . '/role_dt_ajax', 'Gsvadmin\RolesController@dtajax');
	Route::post(config('gsvadmin.adminRoute') . '/save_module_role_permissions/{id}', 'Gsvadmin\RolesController@save_module_role_permissions');
	
	/* ================== Permissions ================== */
	Route::resource(config('gsvadmin.adminRoute') . '/permissions', 'Gsvadmin\PermissionsController');
	Route::get(config('gsvadmin.adminRoute') . '/permission_dt_ajax', 'Gsvadmin\PermissionsController@dtajax');
	Route::post(config('gsvadmin.adminRoute') . '/save_permissions/{id}', 'Gsvadmin\PermissionsController@save_permissions');
	
	/* ================== Departments ================== */
	Route::resource(config('gsvadmin.adminRoute') . '/departments', 'Gsvadmin\DepartmentsController');
	Route::get(config('gsvadmin.adminRoute') . '/department_dt_ajax', 'Gsvadmin\DepartmentsController@dtajax');
	
	/* ================== Employees ================== */
	Route::resource(config('gsvadmin.adminRoute') . '/employees', 'Gsvadmin\EmployeesController');
	Route::get(config('gsvadmin.adminRoute') . '/employee_dt_ajax', 'Gsvadmin\EmployeesController@dtajax');
	Route::post(config('gsvadmin.adminRoute') . '/change_password/{id}', 'Gsvadmin\EmployeesController@change_password');
	
	/* ================== Organizations ================== */
	Route::resource(config('gsvadmin.adminRoute') . '/organizations', 'Gsvadmin\OrganizationsController');
	Route::get(config('gsvadmin.adminRoute') . '/organization_dt_ajax', 'Gsvadmin\OrganizationsController@dtajax');

	/* ================== Backups ================== */
	Route::resource(config('gsvadmin.adminRoute') . '/backups', 'Gsvadmin\BackupsController');
	Route::get(config('gsvadmin.adminRoute') . '/backup_dt_ajax', 'Gsvadmin\BackupsController@dtajax');
	Route::post(config('gsvadmin.adminRoute') . '/create_backup_ajax', 'Gsvadmin\BackupsController@create_backup_ajax');
	Route::get(config('gsvadmin.adminRoute') . '/downloadBackup/{id}', 'Gsvadmin\BackupsController@downloadBackup');
});
