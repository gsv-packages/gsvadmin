<!DOCTYPE html>
<html lang="en">

@section('htmlheader')
	@include('gsvadmin.layouts.partials.htmlheader')
@show
<body class="{{ LAConfigs::getByKey('skin') }} {{ LAConfigs::getByKey('layout') }} @if(LAConfigs::getByKey('layout') == 'sidebar-mini') sidebar-collapse @endif" bsurl="{{ url('') }}" adminRoute="{{ config('gsvadmin.adminRoute') }}">
<div class="wrapper">

	@include('gsvadmin.layouts.partials.mainheader')

	@if(LAConfigs::getByKey('layout') != 'layout-top-nav')
		@include('gsvadmin.layouts.partials.sidebar')
	@endif

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		@if(LAConfigs::getByKey('layout') == 'layout-top-nav') <div class="container"> @endif
		@if(!isset($no_header))
			@include('gsvadmin.layouts.partials.contentheader')
		@endif
		
		<!-- Main content -->
		<section class="content {{ $no_padding or '' }}">
			<!-- Your Page Content Here -->
			@yield('main-content')
		</section><!-- /.content -->

		@if(LAConfigs::getByKey('layout') == 'layout-top-nav') </div> @endif
	</div><!-- /.content-wrapper -->

	@include('gsvadmin.layouts.partials.controlsidebar')

	@include('gsvadmin.layouts.partials.footer')

</div><!-- ./wrapper -->

@include('gsvadmin.layouts.partials.file_manager')

@section('scripts')
	@include('gsvadmin.layouts.partials.scripts')
@show

</body>
</html>
